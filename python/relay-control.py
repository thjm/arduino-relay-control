#!/usr/bin/env python3
#
# relay-control.py
#
"""
Tool for controlling the relays on the 16-relay board via an Arduino Nano
attached via USB (serial).

The RESET line of the Arduino must be grounded via a 10uF capacitor in
order to keep the state of the Arduino when no serial connection is active.
"""

import sys
from time import sleep

import serial

# for parsing command line options
from argparse import ArgumentParser

DEFAULT_SERIAL = '/dev/ttyUSB0'
DEFAULT_BAUD = 9600 # this baudrate is compiled into the Arduino code
SERIAL_TIMEOUT = 0.01
DEFAULT_CHANNEL = 0
DEFAULT_DELAY = 1.0
DEFAULT_VERBOSITY = 1

MODES = ['0', '1', 'on', 'off', 't', 'toggle']

# setup a nice user interface in unix style
parser = ArgumentParser(description=__doc__)

parser.add_argument('-d', '--debug', default=0, type=int,
                    help='set debug level')
parser.add_argument('-s', '--serial', default=DEFAULT_SERIAL, type=str,
                    help='serial interface (%s)' % DEFAULT_SERIAL)

parser.add_argument('-c', '--channel', default=0, type=int,
                    help='relay channel to operate on')
#parser.add_argument('-m', '--mode', default='toggle', type=str,
#                    choices=MODES, help='operating mode for the select channel')
parser.add_argument('mode', metavar='mode', type=str, nargs='?',
                    choices=MODES, help='operating mode for the select channel (1, 0, t, on, off, toggle)')
parser.add_argument('-t', '--time', default=DEFAULT_DELAY, type=float,
                    help='delay time for toggle function (%fs)' % DEFAULT_DELAY)
parser.add_argument('-v', '--verbosity', type=int, default=DEFAULT_VERBOSITY,
                    help='verbosity of the output (%d)' % DEFAULT_VERBOSITY)

args = parser.parse_args()

#

#args.mode = str.lower(args.mode)
#print(args)

if args.channel < 0 or args.channel >= 16:
    print(f'Selected channel {args.channel} is invalid!')
    sys.exit(1)

serial = serial.Serial(args.serial, DEFAULT_BAUD, timeout=SERIAL_TIMEOUT,
                bytesize=8, stopbits=1, parity=serial.PARITY_NONE,
                rtscts=False, xonxoff=False)

if not serial:
    print(f'Could not open serial device f{args.serial}')
    sys.exit(2)

if args.mode == '0' or args.mode == 'off':
    if args.verbosity > 1:
        print(f'Channel {args.channel} off')
    serial.write(f'{args.channel},0\n'.encode())
elif args.mode == '1' or args.mode == 'on':
    if args.verbosity > 1:
        print(f'Channel {args.channel} on')
    serial.write(f'{args.channel},1\n'.encode())
elif args.mode == 't' or args.mode == 'toggle':
    if args.verbosity > 1:
        print(f'Toggle channel {args.channel}')
    serial.write(f'{args.channel},1\n'.encode())
    sleep(0.1)
    serial.write(f'{args.channel},0\n'.encode())
    sleep(args.time)
    serial.write(f'{args.channel},1\n'.encode())
    sleep(0.1)
else:
    print('Wrong mode selected!')

sleep(0.1)

reply = serial.read(200).decode()
if reply and args.verbosity:
    print(f'Reply=\n{reply}')

serial.close()

# eof
