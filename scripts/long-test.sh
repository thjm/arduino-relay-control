#/usr/bin/env bash
#
# long-test.sh
#

SLEEPTIME_INNER=5
SLEEPTIME_OUTER=20

SCRIPT=python/relay-control.py

script=$(dirname $0)/../$SCRIPT

if ! test -x $script; then
  echo "Could not find $SCRIPT! Terminating..."
  exit 1
fi

CHANNELS="0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15"

while [ 1 -gt 0 ]; do
  for ch in $CHANNELS; do
    $script -c $ch -t 1 toggle
    sleep $SLEEPTIME_INNER
  done
  sleep $SLEEPTIME_OUTER
done

# eof
