//
// arduino-relay.ino
//

#include <Wire.h>
#include <Adafruit_MCP23017.h>

// valid address range is 0x20 .. 0x27 -> 0 .. 7
#define MCP_ADDRESS1   0  // 0x20

Adafruit_MCP23017 mcp; // Create MCP 1

#define LED_DEBUGGING
#define LED 13

#define _DEBUG  // enable some debug printout
#define COMMAND_SEPARATOR  ","


/** Mandatory setup() function. */
void setup() {

#ifdef LED_DEBUGGING
  pinMode(LED, OUTPUT);
  //digitalWrite(LED, HIGH);
#endif // LED_DEBUGGING

#if 0
  // wait for serial port to connect. Needed for native USB
  while (!Serial) {
    ;
  }
#endif

  // debug and control input/output
  Serial.begin(9600);

  Wire.begin(); // join I2C/TWI bus (address optional for master)
  // see also: https://github.com/arduino/Arduino/issues/10803
#if defined(WIRE_HAS_TIMEOUT)
  Wire.setWireTimeout(0 /* timeout in usec */, False /* reset on timeout */);
#endif

  mcp.begin(MCP_ADDRESS1);      // Start the (first) MCP

  for ( uint8_t i=0; i<8; ++i ) // Define GPA0 .. GPA7 on MCP1 as outputs
    mcp.pinMode(i, OUTPUT);

  for ( uint8_t i=8; i<16; ++i ) // Define GPB0 .. GPB7 on MCP1 as outputs
    mcp.pinMode(i, OUTPUT);

  // mcp.writeGPIOAB(0xFFFF);
  for ( uint8_t i=0; i<16; ++i )  // set all outputs to HIGH
    mcp.digitalWrite(i, HIGH);

#ifdef LED_DEBUGGING
#if 0
  for ( byte i=0; i<5; ++i) {
    digitalWrite(LED, HIGH);
    delay(500);
    digitalWrite(LED, LOW);
    delay(500);
  }
#endif

  digitalWrite(LED, LOW);
#endif // LED_DEBUGGING
}

String channelStr, actionStr;

/** Mandantory loop() function. */
void loop() {

  int channel = 0;
  int action = -1;

#if 0
  // the built-in method returns also when timeout is hit
  String data = Serial.readStringUntil('\n');
#else
  const int LINE_BUFFER_SIZE = 20; // max line length is one less than this
  char line[LINE_BUFFER_SIZE];
  if (read_line(line, sizeof(line)) < 0) {
    Serial.println("ERR: line too long");
    return; // skip command processing and try again on next iteration of loop
  }

  String data = line;
#endif

  data.trim();

  // sufficient size, at least 3 chars are required incl. separator
  if (!data.length() || data.length() < 3) return;

#ifdef DEBUG
  Serial.print("\""); Serial.print(data); Serial.println("\"");
  Serial.print("length= "); Serial.println(data.length());
#endif // DEBUG

  // get position of separator, if any
  int pos_sep = data.indexOf(COMMAND_SEPARATOR);
  if ( pos_sep < 0 ) return;

#ifdef DEBUG
  Serial.print(" separator @ ");
  Serial.println(pos_sep);
#endif // DEBUG

  channelStr = data.substring(0, pos_sep);
  if (!isValidNumber(channelStr)) return;
  channel = channelStr.toInt();

  actionStr = data.substring(pos_sep+1, data.length());
  if ( !isValidNumber(actionStr)) return;
  action = actionStr.toInt();

  if ( channel < 16 && action >= 0 ) {
    Serial.print("OK: ch "); Serial.print(channel);
    Serial.print(" => action "); Serial.println((action ? "on" : "off"));

    mcp.digitalWrite(channel, (action ? HIGH : LOW));
  }
  else
    Serial.println("ERR: invalid!");
}

/** Check if the passed string contains a valid number.
 *
 *  @return true if valid number, else false.
 */
static boolean isValidNumber(String str) {
  for (byte i=0; i<str.length(); ++i) {
    if (isDigit(str.charAt(i))) return true;
  }

  return false;
}

/** Read a line of characters from the Serial port until the EOL is found.
 *
 *  Source: https://stackoverflow.com/questions/5697047/convert-serial-read-into-a-useable-string-using-arduino
 */
static int read_line(char* buffer, int bufsize) {

  for (int index = 0; index < bufsize; index++) {

    // Wait until characters are available
    while ( !Serial.available() ) ;

    char ch = Serial.read(); // read next character
    Serial.print(ch);        // echo it back: useful with the serial monitor (optional)

    if (ch == '\n') {
      buffer[index] = 0; // end of line reached: null terminate string
      return index;      // success: return length of string (zero if string is empty)
    }

    buffer[index] = ch; // append character to buffer
  }

  // Reached end of buffer, but have not seen the end-of-line yet.
  // Discard the rest of the line (safer than returning a partial line).

  char ch;
  do {
    while ( !Serial.available() ) ;

    ch = Serial.read(); // read next character (and discard it)
    Serial.print(ch);   // echo it back

  } while (ch != '\n');

  buffer[0] = 0; // set buffer to empty string even though it should not be used

  return -1;     // error: return negative one to indicate the input was too long
}
