//
// test1.ino
//

#include <Wire.h>
#include <Adafruit_MCP23017.h>

Adafruit_MCP23017 mcp; // Create MCP 1

void setup() {

  Serial.begin(9600);

  // valid address range is 0x20 .. 0x27
  mcp.begin(0);      // Start the (first) MCP on hardware address 0x20

  mcp.pinMode(0, OUTPUT); // Define GPA0 on MCP1 as output
  mcp.pinMode(1, OUTPUT);
  mcp.pinMode(2, OUTPUT);
  mcp.pinMode(3, OUTPUT);

  mcp.pinMode(8, INPUT); // Define GPB0 on MCP1 as input

}

static unsigned int counter = 0;

void loop() {
  Serial.print("Loop "); Serial.println(++counter, DEC);

#if 0
  int data = mcp.digitalRead(8);
  Serial.print("0x"); Serial.println(data, HEX);
  Serial.print("dataAB= 0x"); Serial.println(mcp.readGPIOAB(), HEX);
  mcp.digitalWrite(0, ~data & 0x01);
#else
  mcp.writeGPIOAB((uint16_t)counter);
  uint16_t data = mcp.readGPIOAB();
  Serial.print("dataAB= 0x"); Serial.println(data, HEX);
#endif

  delay(1000);
}
