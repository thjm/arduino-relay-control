# Arduino controlled relay board

An Arduino controlled relay board.

## Directories

### python

Script intended to control the 16-fold relay board via the Arduino.

From the help page of the script:
```
usage: relay-control.py [-h] [-d DEBUG] [-s SERIAL] [-c CHANNEL] [-t TIME]
                        [-v VERBOSITY]
                        [mode]

Tool for controlling the relays on the 16-relay board via an Arduino Nano
attached via USB (serial). The RESET line of the Arduino must be grounded via
a 10uF capacitor in order to keep the state of the Arduino when no serial
connection is active.

positional arguments:
  mode                  operating mode for the select channel (1, 0, t, on,
                        off, toggle)


optional arguments:
  -h, --help            show this help message and exit
  -d DEBUG, --debug DEBUG
                        set debug level
  -s SERIAL, --serial SERIAL
                        serial interface (/dev/ttyUSB0)
  -c CHANNEL, --channel CHANNEL
                        relay channel to operate on
  -t TIME, --time TIME  delay time for toggle function (1.000000s)
  -v VERBOSITY, --verbosity VERBOSITY
                        verbosity of the output (1)
```

### KiCad

KiCad project and (electrical) scheme files.

### scripts

Some (at present one) scripts for various purposes.

long-test.sh = script which runs an endless loop to test and stress the hardware.

### sketches

The sketch running on the Arduino as well as some test code (also sketches).

## Requirements

### Adafruit MCP23017 library

Clone the Adafruit MCP23017 library from github.com (see references) to the library
folder of your Arduino environment. You will then have a folder named
Adafruit-MCP23017-Arduino-Library with the files Adafruit_MCP23016.h and Adafruit_MCP23016.cpp

The README on the github.com page gives some info about the pin addressing
used by this library.

### Relais board

You need a 16-channel relay board which is branded like
"AZDelivery 16-relay module 12V with optocoupler low level trigger Arduino".

https://www.az-delivery.de/en/products/16-relais-modul?_pos=1&_sid=9d94fc6fa&_ss=r

Alternatives for purchasing this board are at ebay:
https://www.ebay.de/itm/1-16-Kanal-Relais-Optokoppler-Modul-fur-Arduino-Raspberry-Relaiskarte-5-12-230V/353149007504?_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20160811114145%26meid%3D506b68bc42664db3a046f21dcdc89af1%26pid%3D100667%26rk%3D1%26rkt%3D8%26mehot%3Dpp%26sd%3D353149007504%26itm%3D353149007504%26pmt%3D1%26noa%3D1%26pg%3D2334524&_trksid=p2334524.c100667.m2042

Or at Amazon:
https://www.amazon.de/AZDelivery-16-Relais-Optokoppler-Low-Level-Trigger-Arduino/dp/B07N2Z1DWG/ref=sr_1_5?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=relais+platine&qid=1615277228&sr=8-5

The board contains 16 relay which are switched by ULN2803 drivers. Those are
interfaced via PC817 opto couplers to the Arduino. The inputs must be driven
towards GND in order to switch the relay. When the coil of the relay is active,
an additional red LED ist lit near the corresponding relay.

## Notes

### Wiring hints

- connect A0..A2 of the MCP23017 either with GND or Vcc
- connect !RESET of the MCP23017 with Vcc
- Pin 12 (SCL) must be wired to pin A5 of the Arduino
- Pin 13 (SDA) must be wired to pin A4 of the Arduino
- foresee a 10uF capacitor (reasoning see next section) between GND and RESET

### Arduino USB Reset

The arduino resets when you open a serial connection to it because of the "auto-reset" feature.
There are several ways to disable auto-reset:

- Connect a relatively large (10uF) capacitor between the RESET and GROUND on the power header of the board.
- Cut the RESET-EN solder jumper intended for this purpose (not all clones will have this jumper)
- Remove the auto-reset capacitor in between DTR (from the USB/Serial chip) and the RESET signal.

All of these will interfere with the automatic upload of new sketches; you'll need to either
undo them, or manage to use the manual reset button at the right time during upload (you have
about a 2s window).

Source: https://forum.arduino.cc/index.php?topic=558509.msg3809692#msg3809692

### Other observations

- the current consumption when all relay and LEDs are on is approx. 510mA @ 12V

## More Links

- https://www.nikolaus-lueneburg.de/2015/11/mcp23017-i2c-io-port-expander/

- https://github.com/adafruit/Adafruit-MCP23017-Arduino-Library

- https://www.microchip.com/wwwproducts/en/MCP23017
- https://ww1.microchip.com/downloads/en/devicedoc/20001952c.pdf

Alternative to the Adafruit MCP23017 library:
- https://www.arduino.cc/reference/en/libraries/mcp23017/
  -> https://github.com/blemasle/arduino-mcp23017
